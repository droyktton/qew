This code was originally implemented for:  
"Nonsteady relaxation and critical exponents at the depinning transition",  
E. E. Ferrero, S. Bustingorry, and A. B. Kolton  
CONICET, Centro Atómico Bariloche, 8400 San Carlos de Bariloche, Río Negro, Argentina  
Phys. Rev. E 87, 032122 (2013)  
http://link.aps.org/doi/10.1103/PhysRevE.87.032122  
http://arxiv.org/abs/1211.7275  
  

The code is freely available under GNU GPL v3.  
  

A Supplemental Material was published together with the paper,  
describing the code and giving some benchmarkings.  
http://pre.aps.org/supplemental/PRE/v87/i3/e032122  
You can find it also in this repository as SM.pdf  


